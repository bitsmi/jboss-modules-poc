package com.bitsmi.jboss.modules.poc.maven;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.interpolation.util.StringUtils;

@Mojo(name = "module:copy", requiresDependencyResolution=ResolutionScope.COMPILE)
public class ModuleCopyDepsMojo extends AbstractMojo 
{
	@Component
	protected MavenProject project;

	@Parameter(defaultValue="${project.build.directory}/modules", readonly=true)
	private File outputDir;
	
    public void setProject(MavenProject project)
    {
    	this.project = project;
    }
    
    /**
     * {@inheritDoc}
     */
	public void execute() throws MojoExecutionException, MojoFailureException
    {
		getLog().info("JBoss Modules module:copy");
		
		Set<Artifact> artifacts = project.getArtifacts();
		getLog().info("Copiando artifacts a " + outputDir);
		for (Artifact artifact:artifacts){
			getLog().info("Tratando artifact " + artifact.getGroupId() + ":" + artifact.getArtifactId() + ":" + artifact.getVersion());
			try{
				copyFile(artifact);
			}
			catch(IOException e){
				// Unexpected error
				throw new MojoExecutionException(e.getMessage(), e);
			}
		}
    }
	
	private void copyFile(Artifact artifact) throws IOException
	{
		StringBuilder destRelativeFile = new StringBuilder()
				.append(StringUtils.replace(artifact.getGroupId(), ".", "/"))
				.append("/").append(artifact.getArtifactId())
				.append("/main/")
				.append(artifact.getFile().getName());
		
		File destAbsoluteFile = new File(outputDir, destRelativeFile.toString());
		
		getLog().info("Copiando fichero " + artifact.getFile().getAbsolutePath() + " -> " + destAbsoluteFile.getAbsolutePath());
		FileUtils.copyFile(artifact.getFile(), destAbsoluteFile);
	}
}
