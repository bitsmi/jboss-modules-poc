package com.bitsmi.jboss.modules.poc.main;

import java.util.ServiceLoader;

import org.jboss.modules.Module;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bitsmi.jboss.modules.poc.spi.IPluginService;

/** 
 * Punto de entrada de la aplicación. Para este ejemplo, sólamente se encargará de recuperar
 * todas las instancias de un servicio determinado de todos los plugins disponibles y ejecutar
 * una acción sobre cada una de ellas.
 * @author Antonio Archilla Nava
 */
public class MainProgram 
{
    public static void main(String... args)
    {
        // Mediante la configuración de los módulos del framework de logueo se podrá utilizar de forma normal
        Logger log = LoggerFactory.getLogger(MainProgram.class);
        log.info("Recibiendo mensajes desde los plugins...");
        
        /* Mediante el mecanismo de ServiceLoader estándar de Java obtenemos todas las instancias 
		 * de un mismo servicio proporcionadas por cada uno de los plugins de la aplicación
		 */
        ServiceLoader<IPluginService> services = Module.getCallerModule().loadService(IPluginService.class);
        for(IPluginService service:services){
			// Para cada uno de ellos ejecutamos una acción 
        	log.info("MSG: {}", service.getMessage());
        }
    }
}
