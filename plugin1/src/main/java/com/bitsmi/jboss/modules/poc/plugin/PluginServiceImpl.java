package com.bitsmi.jboss.modules.poc.plugin;

import com.bitsmi.jboss.modules.poc.spi.IPluginService;

public class PluginServiceImpl implements IPluginService
{
    @Override
    public String getMessage()
    {
        return "Hello from plugin 1!!";
    }            
}
